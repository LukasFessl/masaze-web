<?php

namespace FrontModule;

use Nette;




class HomepagePresenter extends BasePresenter
{



    public function beforeRender()
    {
        parent::beforeRender();
    }



    public function actionDefault($slug = null)
    {
    	
    }

    public function renderDefault($slug = null)
    {
    	$post = NULL;
    	if (!is_null($slug)) {
    		$post = $this->factory->createPost()->findBySlug($slug);
    		if(is_null($post->id))
    			throw new Nette\Application\BadRequestException;
    	} else {
    		$post = $this->factory->createPost()->findAllOrderLimit('rank ASC', array(0, 1));
    		$post = $post[0];
            $this->redirect("Homepage:Default", $post->slug);
    	}

    	$this->template->post = $post;	
    }



}
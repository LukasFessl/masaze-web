<?php

namespace FrontModule;

use Nette;




class BasePresenter extends Nette\Application\UI\Presenter
{
	protected $factory;



	public function startup()
	{
		parent::startup();
		$this->factory = $this->context->factory;
	}



	public function beforeRender()
	{
		parent::beforeRender();

        $menu =  array();
        $posts = $this->factory->createPost()->findAllByDisplayOrder(1, 'rank ASC');
        foreach ($posts as $post) {
            $menu[$post->name] = array('title' => $post->name, 'link' => 'Homepage:default', 'parameter' => array('slug' => $post->slug));
        }

        $this->template->menu = $menu;
        $this->template->head = $this->factory->createHead()->findBySelected(1);
        $settings = $this->factory->createSettings()->findAll();
		$settings = $settings[0];
		$css = "";
		if($settings->marginTop)
			$css .= "margin-top:".$settings->marginTop.";";
		if($settings->marginLeft)
			$css .= "margin-left:".$settings->marginLeft.";";
		if($settings->marginRight)
			$css .= "margin-right:".$settings->marginRight.";";
		if($settings->marginBottom)
			$css .= "margin-bottom:".$settings->marginBottom.";";
		if($settings->color)
			$css .= "color:".$settings->color.";";
		if($settings->fontFamily)
			$css .= "font-size:".$settings->fontSize.";";
		if($settings->fontSize)
			$css .= "font-family:".$settings->fontFamily.";";
		if($settings->imageBackground)
			$css .= "background-image: url(/upload/files/".$settings->imageBackground.");";
		$this->template->css = $css;
									
	}


	
}
<?php

namespace AdminModule;

use Nette;




class HeadPresenter extends BasePresenter
{

	protected $factory;


	public function startup()
	{
		parent::startup();
	}



	public function beforeRender()
	{
		parent::beforeRender();
	}



	public function actionDefault()
	{

	}



	public function renderDefault()
	{
		$this->template->heads = $this->factory->createHead()->findAllOrder('id DESC');
	}


	public function createComponentHeadForm()
	{
		return new HeadForm($this->factory);
	}



	public function handleDelete($id)
	{
		if ($this->isAjax()) {
			$file = $this->factory->createHead()->findById($id);
			if (file_exists("upload/head/".$file->slug))
				unlink("upload/head/".$file->slug);
			$file->delete();
			$this->invalidateControl('headList');
		}
	}



	public function handleSetHead($id)
	{
		if ($this->isAjax()) {
			$head = $this->factory->createHead()->findBySelected(1);
			if ($head->id) {
				$head->selected = 0;
				$head->save();
			}

			$head = $this->factory->createHead()->findById($id);
			$head->selected = 1;
			$head->save(); 
			$this->invalidateControl('headList');
		}
	}

}
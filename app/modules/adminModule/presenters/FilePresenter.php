<?php

namespace AdminModule;

use Nette;




class FilePresenter extends BasePresenter
{

	protected $factory;


	public function startup()
	{
		parent::startup();
	}



	public function beforeRender()
	{
		parent::beforeRender();
	}



	public function actionDefault()
	{

	}



	public function renderDefault()
	{
		$this->template->files = $this->factory->createFile()->findAllOrder('id DESC');
	}



	public function createComponentFileForm()
	{
		return new FileForm($this->factory);
	}


	public function handleDelete($fileId)
	{
		if ($this->isAjax())
		{
			$file = $this->factory->createFile()->get($fileId);
			if (file_exists("upload/files/".$file->slug))
				unlink("upload/files/".$file->slug);
			$file->delete();
			$this->invalidateControl('fileList');

		}
	}


}
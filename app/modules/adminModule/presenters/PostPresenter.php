<?php

namespace AdminModule;

use Nette;




class PostPresenter extends BasePresenter
{

    protected $factory;


    public function startup()
    {
        parent::startup();
    }



    public function beforeRender()
    {
        parent::beforeRender();

        $subMenu =  array('Nastavení záložek' => array('title' => 'Nastavení záložek', 'link' => 'Post:settings'),
                          'Vytvořit' => array('title' => 'Vytvořit', 'link' => 'Post:create'));
        $posts = $this->factory->createPost()->findAllByDisplayOrder(1, 'rank ASC');
        foreach ($posts as $post) {
            $subMenu[$post->name] = array('title' => $post->name, 'link' => 'Post:default', 'parameter' => array('id' => $post->id));
        }

        $this->template->subMenu = $subMenu;
    }



    public function actionDefault($id = NULL)
    {
        
    }



    public function renderDefault($id = NULL)
    {
        $this->template->id = $id;
    }



    public function createComponentMceForm()
    {
        $post = $this->factory->createPost()->findById($this->getParameter('id'));
        return new MceForm($post);
    }



    public function createComponentPostForm()
    {
        $postForm = new PostForm($this->factory);
        return $postForm;
    }


    public function handleGetFiles()
    {
        if ($this->isAjax()) {
            $this->payload->files = $this->factory->createFile()->findAll();
            $this->terminate();
        }
    }



    public function renderSettings()
    {
        $this->template->posts = $this->factory->createPost()->findAllOrder('rank ASC');
    }



    public function handleDeletePost($id)
    {
        if ($this->isAjax()) {
            $this->factory->createPost()->delete($id);
            $this->invalidateControl('submenu');
            $this->invalidateControl('postList');
        }
    }



    public function handleDisplay($id)
    {
        if ($this->isAjax()) {
            $post = $this->factory->createPost()->get($id);
            if ($post->display == 1)
                $post->display = 0;
            else 
                $post->display = 1;
            $post->save();
            $this->invalidateControl('postList');
            $this->invalidateControl('submenu');
        }
    }



    public function handleUp($id)
    {
        if ($this->isAjax()) {
            $thisPost = $this->factory->createPost()->get($id);
            if ($thisPost->rank > 1) {
                $nextPost = $this->factory->createPost()->findByRank($thisPost->rank - 1);
                $nextPost->rank +=1;
                $nextPost->save();
                $thisPost->rank -= 1;
                $thisPost->save(); 
            }
            $this->invalidateControl('submenu');
            $this->invalidateControl('postList');
        }
    }



    public function handleDown($id)
    {
        if ($this->isAjax()) {
            $lastPost = $this->factory->createPost()->findAllOrderLimit('rank DESC', array(0, 1));
            $lastPost = $lastPost[0];
            $thisPost = $this->factory->createPost()->get($id);
            if ($lastPost->rank > $thisPost->rank) {
                $nextPost = $this->factory->createPost()->findByRank($thisPost->rank + 1);
                $nextPost->rank -= 1;
                $nextPost->save();
                $thisPost->rank += 1;
                $thisPost->save(); 
            }
            $this->invalidateControl('submenu');
            $this->invalidateControl('postList');
        } 
    }

}
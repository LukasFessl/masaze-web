<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;

class AuthPresenter extends BasePresenter
{
	public function startup()
	{
		parent::startup();
		if($this->action == "logout")
			$this->_logout();
	}

	public function actionDefault()
	{
		$this->setLayout('authLayout');
	}



	public function renderDefault()
	{

	}



	public function createComponentAuthForm()
	{
		$form = new Form();
		$form->addText('email', 'Login: ');
		$form->addPassword('password', 'Pssword: ');
		$form->addSubmit('login', 'Přihlásit');
		$form->onSuccess[] = $this->authProcess;

		return $form;
	}



	public function authProcess($form)
	{
		$authenticator = $this->context->authenticator;
		$val = $form->getValues();

		$user = $this->getUser();
		$user->setAuthenticator($authenticator);

		try {
			$user->login($val->email, $val->password);	
			$user->setExpiration('120 minutes', FALSE);	
			$this->redirect("Homepage:default");
		} catch (AuthenticationException $e) {
			if ($e->getCode() == IAuthenticator::INVALID_CREDENTIAL) {
				// $form->addError("Nesprávné jméno nebo heslo");
				$this->flashMessage("Nesprávné jméno nebo heslo");
			}
			else
				$form->addError("Nastala neznámá chyba !!!");
		}
		
	}




	protected function _logout()
	{
		$user = $this->getUser();
		$user->logout(TRUE);
		$this->redirect('auth:default');
	}


	public function handleLogout()
	{
		$user = $this->getUser();
		$user->logout(TRUE);
		$this->redirect('auth:default');
	}

}
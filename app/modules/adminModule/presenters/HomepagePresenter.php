<?php

namespace AdminModule;

use Nette;




class HomepagePresenter extends BasePresenter
{

    public function beforeRender()
    {
        parent::beforeRender();
    }



    public function actionDefault($logout = NULL)
    {
        if ($logout) {
            $user = $this->getUser();
            $user->logout(TRUE);
            $this->redirect('this');
        }

    } 


}
<?php

namespace AdminModule;

use Nette;




class BasePresenter extends Nette\Application\UI\Presenter
{
    protected $factory;

    public function startup()
    {
        parent::startup();
        $this->factory = $this->context->factory;

        $user = $this->getUser();
        if ($this->name != "Admin:Auth") {
            if (!$user->isLoggedIn())
                $this->redirect('Auth:default');
            else {
                if ($user->isInRole('user')) {
                    $this->redirect('Auth:default');
                }    
            }
        }else{
            if ($user->isLoggedIn()) {
                if ($user->isInRole('admin')) {
                    $this->redirect('Post:default');
                }
            }
                
        }
    }


    public function beforeRender()
    {
        $menu =  array( 
                    'Hlavička' =>   array('title'=>'Hlavička', 'class'=>'',        'link'=>'Head:default',               'current'=>'Head:*',    'presenter'=>'Admin:Head',     'src'=>'images/icon_head.png'),
                    'Záložky' =>    array('title'=>'Záložky',  'class'=>'',        'link'=>'Post:settings',              'current'=>'Post:*',    'presenter'=>'Admin:Post',     'src'=>'images/icon_category.png'),
                    'Soubory' =>    array('title'=>'Soubory',  'class'=>'',        'link'=>'File:default',               'current'=>'File:*',    'presenter'=>'Admin:File',     'src'=>'images/icon_file.png'),
                    'Uživatel' =>   array('title'=>'Uživatel', 'class'=>'',        'link'=>'User:list',                  'current'=>'User:*',    'presenter'=>'Admin:User',     'src'=>'images/icon_user.png'),
                    'Nastavení' =>  array('title'=>'Nastavení','class'=>'',        'link'=>'Settings:default',           'current'=>'Settings:*','presenter'=>'Admin:Settigns', 'src'=>'images/icon_settings.png'),
                    'Adminer' =>    array('title'=>'Adminer',  'class'=>'',        'link'=>'adminer-4.1.0.php',          'current'=>'Head:*',    'presenter'=>'Admin:default',  'src'=>'images/icon_adminer.png'),
                    'Odhlásit' =>   array('title'=>'Odhlásit', 'class'=>'lognout', 'link'=>'Homepage:default?logout=ok', 'current'=>'xx:*','presenter'=>'Admin:Homepage', 'src'=>'images/icon_logout.png'));  
    
        $this->template->menu = $menu;
    }
}
<?php

namespace AdminModule;

use Nette;
use Bean\ORM;
use Model\User;


class UserPresenter extends BasePresenter
{
    protected $factory;


    public function startup()
    {
        parent::startup();  
    }
    


    public function beforeRender()
    {
        parent::beforeRender();
        $subMenu =  array(  
                    'Přehled'           => array('title' => 'Přehled',           'link' => 'User:list'),
                    'Vytvořit uživatele'=> array('title' => 'Vytvořit uživatele','link' => 'User:create'));

        $this->template->subMenu = $subMenu;
    }



    public function renderList()
    {
        // dump($this->context);
        // $this->template->users = $this->user->get();
        // dump($this->factory->createUser()->findByNotNote('assaasd'));
        // dump($this->factory->createUser()->findAllByEmailAndNotIdAndFirstNameOrder('john', 1));
        $this->template->users = $this->factory->createUser()->findAllOrder('id');
        // dump($this->factory->createUser('Entt\\'));
        // dump($this->factory->createUser(5));
        // dump($this->factory->createUser()->get(1)->getTableName());
        // $this->template->users = array();
    }



    public function createComponentUserForm()
    {
        $user = $this->factory->createUser()->findById($this->getParameter('id'));
        return new UserForm($user);
    }



    public function handleDelete($userId)
    {
        if ($this->isAjax()) {
            $this->factory->createUser()->delete($userId);
            $this->invalidateControl('userList');
        }
    }





    /**
    * $id - user id
    **/
    public function renderEdit($id)
    {

    }


}
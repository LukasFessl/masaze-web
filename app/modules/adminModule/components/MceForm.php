<?php
namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

class MceForm extends Control
{

	private $post;

	public function __construct($post)
	{
		$this->post = $post;
	}

	public function createComponentForm()
	{
		$form = new Form();
		$form->addText('name', 'Název: ')->setValue($this->post->name);
		$form->addTextArea('text','Text: ')->setHtmlId('textarea_mce')->setValue($this->post->text);
		$form->addTextArea('description','Popis: ')->setHtmlId('textarea_description')->setValue($this->post->description);
		$form->addTextArea('keyWord','Klíčová slova: ')->setHtmlId('textarea_keyWord')->setValue($this->post->keyWord);
		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = $this->processMce;

		return $form;
	}




	public function processMce($form)
	{
		$val = $form->getValues();
		$this->post->bind($val);
		$this->post->slug = Strings::webalize($val->name);
		$this->post->save();
	}





	public function render()
	{
		$this->template->setFile(__DIR__.'/GeneralForm.latte');
		$this->template->render();
	}

}
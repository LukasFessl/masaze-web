<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Nette\Image;

class SettingsForm extends Control
{
	private $settings;


	public function __construct($settings)
	{
		$this->settings = $settings;
	}



	public function createComponentForm()
	{
		$settings = $this->settings;
		$form = new Form();
		$form->addText('marginLeft', 'Mezera z leva: ', '10')->setValue($settings->marginLeft);
		$form->addText('marginRight', 'Mezera z prava: ', '10')->setValue($settings->marginRight);
		$form->addText('marginTop', 'Mezera ze zhora: ', '10')->setValue($settings->marginTop);
		$form->addText('marginBottom', 'Mezera ze zdola: ', '10')->setValue($settings->marginBottom);
		$form->addText('color', 'Barva písma: ', '20')->setValue($settings->color);
		$form->addText('fontSize', 'Velikost písma: ', '20')->setValue($settings->fontSize);
		$form->addText('fontFamily', 'Druh písma: ', '20')->setValue($settings->fontFamily);
		$form->addText('imageBackground', 'Pozadí pod tlačítkem: ', '20')->setValue($settings->imageBackground);
		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = $this->settingsProcess;
		return $form;
	}



	public function settingsProcess($form)
	{
		$val = $form->getValues();
		$this->settings->bind($val);
		$this->settings->save();
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/GeneralForm.latte');
		$this->template->render();
	}
}
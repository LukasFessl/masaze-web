<?php
namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

class UserForm extends Control
{

	private $user;

	public function __construct($user = NULL)
	{
		$this->user = $user;
	}

	public function createComponentUserForm()
	{
		$form = new Form();

		$form->addText('email', 'E-mail:')->setValue($this->user->email)->getControlPrototype();
		
		$form->addSelect('role', 'Role', array(
		    'admin' => 'Administrátor'
		));
		$form['role']->setDefaultValue($this->user->role ? $this->user->role : 'admin');

		$form->addSelect('enabled', 'Účet', array(
		    '1' => 'Povolen',
		    '0' => 'Zablokován',
		));
		$form['enabled']->setDefaultValue($this->user->enabled ? $this->user->enabled : 1);

		if($this->user->id)
		{
			$form->addSubmit('updateUser', 'Změnit');
			$form->onSuccess[] = $this->processUser;
		}

		$form->addPassword('password', 'Heslo:');
		$form->addPassword('retipePassword', 'Heslo znovu:');

		if($this->user->id)
		{
			$form->addSubmit('updatePassword', 'Změnit heslo');
			$form->onSuccess[] = $this->processPassword;
		}
		else
		{
			$form->addSubmit('saveUser', 'Uložit');
			$form->onSuccess[] = $this->processAllUser;
		}
		
	
		return $form;
	}


	public function processAllUser($form)
	{
		if($form['saveUser']->isSubmittedBy())
		{
			$val = $form->getValues();

			if($val->password == $val->retipePassword && $val->password && $val->email)
			{
				if(!$this->user->emailExist($this->user->id, $val->email))
				{
					unset($val['retipePassword']);
					$this->user->bind($val);
					$this->user->setPassword($val->password);
					$this->user->save();	
					$this->presenter->redirect('User:list');
				}
				else
				{
					$form['email']->addError("Tento email již existuje");
				}
			}
			else 
			{
				if($val->password != $val->retipePassword)
				{
					$form['password']->addError("Hesla se neshodují");
					$form['retipePassword']->addError("Hesla se neshodují");
				}
				else if(!$val->password)
				{
					$form['password']->addError("Heslo musí být vyplněno");
					$form['retipePassword']->addError("Heslo musí být vyplněno");
				}	

				if(!$val->email)
					$form['email']->addError("Email musí být vyplněn");
			}
		}
		
	}



	public function processUser($form)
	{
		if($form['updateUser']->isSubmittedBy())
		{
			$val = $form->getValues();	
			if($val->email)
			{
				if(!$this->user->emailExist($this->user->id, $val->email))
				{	
					unset($val['password']);
					unset($val['retipePassword']);
					$this->user->bind($val);
					$this->user->save();	
					$this->flashMessage("Změny uloženy");
				}
				else
				{
					$form['email']->addError("Tento email již existuje");
				}
					
			}
			else
			{
				$form['email']->addError("Email musí být vyplněn");
			}
		}
	}



	public function processPassword($form)
	{
		if($form['updatePassword']->isSubmittedBy())
		{
			$val = $form->getValues();
			if ($val->password && $val->retipePassword)
			{
				if($val->password == $val->retipePassword)
				{
					$this->user->setPassword($val->password);
					$this->user->save();
					$this->flashMessage("Změny uloženy");
				}
				else 
				{
					$form['password']->addError("Hesla se neshodují");
					$form['retipePassword']->addError("Hesla se neshodují");
				}
			}
			else
			{
					$form['password']->addError("Heslo musí být vyplněno");
					$form['retipePassword']->addError("Heslo musí být vyplněno");
			}
		}
	}


	public function render()
	{
		$this->template->setFile(__DIR__.'/UserForm.latte');
		$this->template->render();
	}

}
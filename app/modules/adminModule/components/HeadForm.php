<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Nette\Image;

class HeadForm extends Control
{
	private $factory;


	public function __construct($factory)
	{
		$this->factory = $factory;
	}



	public function createComponentForm()
	{
		$form = new Form();
		$form->addText('name', 'Název ', '30');
		$form->addUpload('file', '', true);
		$form->addSubmit('upload', 'Vložit');
		$form->onSuccess[] = $this->fileProcess;
		return $form;
	}



	public function fileProcess($form)
	{
		$val = $form->getValues();
		$time = time();
		if (count($val->file) == 1)
		{
			$name = "";
			$val->name ? $name = $val->name : $name = $val->file[0]->name;
			
			$slug = "";
			$match = Strings::match($val->file[0]->name, '~\.([a-z0-9]{2,7})$~i');
			if ($val->name)
			{
				$slug = Strings::webalize($name)."_".time().$match[0];
				$name = $name.$match[0];
			}
			else
			{
				$slug = preg_replace_callback('~(-)([a-z0-9]{2,7})$~i', function($m) use($time){
					return "_".$time.".".$m[2];
				}, Strings::webalize($name));
			}

			// dump("match:".$match[0]);
			// dump("name:".$name);
			// dump("slug:".$slug);

			$file = $this->factory->createHead();
			$file->setName($name);
			$file->setSlug($slug);
			$file->selected = 0;
			$file->save();
			$dir = 'upload/head/'.$slug;
			$val->file[0]->move($dir);
			$this->redirect('this');

		}
		else if (count($val->file) > 1)
		{
			// $val = $form->getValues();
			// $time = time();
			// for ($i=0; $i < count($val->file); $i++) {
			// 	$slug = $time.'_'.$i.'_'.\ORM\Product_image::toFileName($val->file[$i]->name);
			// 	$dir = 'upload/catalog/'.$user->maskName.'/images/'.$slug;
			// 	$val->file[$i]->move($dir);

			// 	$pI = $this->productImage->create();
			// 	// $pI->bind(['name'=>$val->file[$i]->name, 'slug'=>$slug, 'productId'=>$this->product->id]);
			// 	$pI->bind(array('name'=>$val->file[$i]->name, 'slug'=>$slug, 'productId'=>$this->product->id));
			// 	$pI->save();

			// 	$smallImage = $this->_imageResize($dir,'upload/catalog/'.$user->maskName.'/thumb/');
   //          	$smallImage->save('upload/catalog/'.$user->maskName.'/thumb/'.$slug);
			// }
			dump($val);
		}
		
	}




	public function render()
	{
		$this->template->setFile(__DIR__.'/GeneralForm.latte');
		$this->template->render();
	}
}
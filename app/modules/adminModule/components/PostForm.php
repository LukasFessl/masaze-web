<?php
namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;


class PostForm extends Control
{

	private $factory;
	private $post;

	public function __construct($factory = NULL)
	{
		$this->factory = $factory;
		$this->post = $this->factory->createPost();
	}

	public function createComponentForm()
	{
		$form = new Form();
		$form->addText('name', 'Název')->setValue($this->post->name)->getControlPrototype();
		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = $this->processPost;	
		
		return $form;
	}





	public function processPost($form)
	{
		$val = $form->getValues();
		$this->post->bind($val);
		$this->post->slug = Strings::webalize($val->name);
		$this->post->display = 1;

		$p = $this->factory->createPost()->findAllOrderLimit('rank DESC', array(0,1));
		$this->post->rank = $p[0]->rank+1;
		$this->post->save();
	}


	public function render()
	{
		$this->template->setFile(__DIR__.'/GeneralForm.latte');
		$this->template->render();
	}

}
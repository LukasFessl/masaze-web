<?php

namespace AdminModule;

use Nette;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;

class Authenticator implements IAuthenticator
{

	protected $connection;

	public function __construct(Nette\Database\Context $connection)
	{
		$this->connection = $connection;
	}



	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		$res = $this->connection->table('user')
								->where(array('email' => $email, 'password' => HashPassword::hash($password)))
								->select('id, email, password, role')->fetch();
		if(!$res)
			throw new AuthenticationException('User or password is bad', IAuthenticator::INVALID_CREDENTIAL);

		return new Identity($res->id, $res->role, $res);
	}
}
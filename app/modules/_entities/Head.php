<?php

namespace Model;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Head extends Entity
{
	public $id = NULL;
	public $name;
	public $slug;
	public $selected;
	
	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'dateCreated' => array('timeStamp'=>true),
			'lastUpdated' => array('timeStamp'=>true)
		);
		return $mapping;
	}
}
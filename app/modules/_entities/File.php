<?php

namespace Model;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class File extends Entity
{
	public $id = NULL;
	public $name;
	public $slug;
	
	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'dateCreated' => array('timeStamp'=>true),
			'lastUpdated' => array('timeStamp'=>true)
		);
		return $mapping;
	}



	protected function beforeSave()
	{
		// $this->lastUpdated = date('Y-m-d H:i:s');
	}

	
}
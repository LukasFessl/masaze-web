<?php

namespace Model;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Post extends Entity
{
	public $id = NULL;
	public $name;
	public $slug;
	public $text;
	public $description;
	public $keyWord;
	public $display;
	public $rank;
	
	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'dateCreated' => array('timeStamp'=>true),
			'lastUpdated' => array('timeStamp'=>true)
		);
		return $mapping;
	}

}
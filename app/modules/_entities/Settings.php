<?php

namespace Model;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Settings extends Entity
{
	public $id = NULL;
	public $marginLeft;
	public $marginRight;
	public $marginTop;
	public $marginBottom;
	public $color;
	public $fontSize;
	public $fontFamily;
	public $imageBackground;
	
	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'dateCreated' => array('timeStamp'=>true),
			'lastUpdated' => array('timeStamp'=>true)
		);
		return $mapping;
	}

}
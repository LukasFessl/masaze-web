-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `head`;
CREATE TABLE `head` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `selected` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci,
  `description` text COLLATE utf8_czech_ci,
  `key_word` text COLLATE utf8_czech_ci,
  `display` tinyint(1) NOT NULL,
  `rank` smallint(3) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `margin_left` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `margin_right` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `margin_top` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `margin_bottom` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `color` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `font_size` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `font_family` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `image_background` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  `role` enum('admin') COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2014-11-14 17:57:20
<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->addDirectory(__DIR__ . '/../vendor/others')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');


// $configurator->onCompile[] = function ($configurator, $compiler) {
//     $compiler->addExtension('bean', new Bean\ORM\MyBeanExtension);
// };

// $database = array(
// 				'dns' => 'mysql:host=localhost;dbname=czvyrobky',
// 				'user' => 'root',
// 				'password' => ''
// 				);
// $entityNamespace = '\Model';
// $nameBuilder = 'Bean\ORM\TestBuilder';

// \Bean\ORM\BeanDatabase::setDatabase($database, $entityNamespace);


// \Bean\ORM\BeanDatabase::$connection = new Nette\Database\Context(new Nette\Database\Connection('mysql:host=localhost;dbname=czvyrobky','root',''));

$container = $configurator->createContainer();

return $container;

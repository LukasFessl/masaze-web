$( document ).ready(function() {


    tinymce.init({
        relative_urls: false,

        mode : "exact",
        elements : "textarea_mce",
        resize: "both",
        menubar : false,
        width :700,
        height: 300,
        content_css : '../../css/tinymce.css',
        
        plugins: "code link image advlist textcolor anchor",
        // toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | table | cut copy paste | link unlink anchor image media code",
        // toolbar2: "formatselect fontselect fontsizeselect | bullist numlist | outdent indent blockquote |  inserttime preview | forecolor backcolor",
        toolbar1: "formatselect | fontselect | fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | inserttime preview",
        toolbar2: "outdent indent blockquote | cut copy paste | link unlink anchor image code | forecolor backcolor | bullist numlist ",

        tools: "inserttable" ,
        fontsize_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 28px 36px",

         file_browser_callback : function(field_name, url, type, win) { 
             fileBrowser(field_name, url, type, win);
        },
    });
     
})


 function fileBrowser(field_name, url, type, win) {

        $("#fileBrowser").remove();
        $(".mce-window").append("<div id='fileBrowser'></div>");

        $.get('/admin/post/default?do=getFiles', {})
            .done(function(data){
                files = data.files;
                for (var i = 0; i < files.length; i++) {
                    $("#fileBrowser").append('<span class="ftwrap"><a class=fbButton href=# onclick=set("'+field_name+'","'+files[i].name+'","'+files[i].slug+'")>'+files[i].name+'</a>'+
                        '<a class="fbshow" target=_blank href=/upload/files/'+files[i].slug+' onclick=stop() title=Náhled></a></span>');
                };
            }); 

        event.stopPropagation();
    }


function set(field_name, name, slug)
{
    var url = "/upload/files/"+slug;
    $("#"+field_name).val(url);
     // event.stopPropagation();
}

$('html').click(function() {
    $("#fileBrowser").remove();
});


function stop()
{
    event.stopPropagation();
}